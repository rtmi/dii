package remote

import (
	"context"
	"errors"
)

type Service interface {
	AttachHandler(f func([]byte) error)
	Close() error
	Connect(ctx context.Context) chan error
	Join(ctx context.Context, c string) error
	Chat(ctx context.Context, c string, m string) error
	Names(ctx context.Context, c string) error
	State() string
}

var (
	// ErrIrcConnected is returned when a live connection exists.
	ErrIrcConnected = errors.New("Connection Exists, disconnect first before making another connect call.")

	// ErrChannelJoined is returned when a join request is repeated on the same channel.
	ErrChannelJoined = errors.New("Current Channel, part channel first before making another join call.")

	// ErrDetached is returned when a chat request is made outside channel.
	ErrDetached = errors.New("Chat Limbo, join channel first before chatting.")
)

const CONNECTION_INIT = "Created"
const CONNECTION_LIVE = "ConnectionLive"
