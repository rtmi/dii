package remote

import (
	"bytes"
	"fmt"
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

// Simple action to print message on stdout
func printer(message []byte) error {
	m := bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
	fmt.Println(string(m))
	return nil
}
