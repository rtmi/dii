/*
   A way of IRC remote control:
   wrapping the ii IRC client by
   Suckless Tools. Another descr,
   this is the ii driver of the
   chat service interface.
*/
package remote

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
	"strings"
	"syscall"
	"time"

	"github.com/pelletier/go-toml"
)

// Send join channel request to input FIFO file
func (r *basicService) Join(ctx context.Context, ch string) error {
	k := strings.ToLower(ch)
	if _, ok := r.channels[k]; ok {
		return nil
	}
	r.channels[k] = 1

	req := fmt.Sprintf("/j %s\n", k)
	if err := r.writeToServer(req); err != nil {
		return err
	}
	go r.inbound(ctx, k)
	return nil
}

// Reads from rcv FIFO for incoming chat messages
func (r *basicService) inbound(_ context.Context, ch string) error {
	// There will be one go routine for each join/channel because each FIFO out requires a "tail-er"

	npipe := path.Join(r.home, "irc", r.server, ch, "out")

	if _, err := os.Stat(npipe); os.IsNotExist(err) {
		fmt.Println("Delay 30 secs before reading from: ", npipe)
		time.Sleep(30 * time.Second)
		if _, err = os.Stat(npipe); os.IsNotExist(err) {
			return fmt.Errorf("Out FIFO not found: %s", npipe)
		}
	}

	op, err := os.OpenFile(npipe, syscall.O_RDONLY, os.ModeNamedPipe)
	if err != nil {
		return err
	}
	defer op.Close()
	reader := bufio.NewReader(op)

	for {
		buf, err := reader.ReadBytes('\n')
		if err != nil {
			// print buffer read up to error
			r.fmtMessage(buf, ch)
			if err != io.EOF {
				return fmt.Errorf("%s out fifo error: %v", npipe, err)
			}

			// Too verbose, is there a better msg?
			////fmt.Println("EOF found, attempt reader reset: ", npipe)
			time.Sleep(10 * time.Second)
			reader.Reset(op)
		} else {
			r.fmtMessage(buf, ch)
			time.Sleep(800 * time.Millisecond)
		}
	}

	return nil
}
func (r *basicService) fmtMessage(buf []byte, ch string) {
	// Prefix with channel name
	// and remove whitespace
	t := bytes.TrimSpace(buf)
	if len(t) == 0 {
		return
	}
	// Disregard ticks prefix
	chop := t
	if i := bytes.IndexRune(t, ' '); i != -1 {
		chop = t[i+1:]
	}
	s := [][]byte{
		[]byte(ch),
		chop,
	}
	r.onMessage(bytes.Join(s, []byte(".")))
}

/*
// Keep the channel name as the prefix of the chat text
func formatLine(ch string, line string) []byte {
	// TODO opportunity to color background of text?

	l := fmt.Sprintf("%s.%s", ch, line)
	return []byte(l)
}
*/
func (r *basicService) Chat(_ context.Context, ch string, m string) error {
	//low := strings.ToLower(ch)

	// TODO echo "m" > ~/irc/r.server/ch/in WHERE the input FIFO is different from the server FIFO

	return nil
}

func (r *basicService) Names(_ context.Context, ch string) error {
	//low := strings.ToLower(ch)

	// TODO echo "/who" > ~/irc/r.server/ch/in

	return nil
}

// Add handler for incoming messages
func (r *basicService) AttachHandler(f func([]byte) error) {
	r.onMessage = f
}

// Run the ii command to connect to IRC network
func (r *basicService) Connect(ctx context.Context) chan error {
	// IRC network connection remoting opens and doesn't return
	// so goroutine is required
	errc := make(chan error)
	go func() {
		defer close(errc)
		path := "/bin/ii"

		args := []string{"-s", r.server, "-n", r.nickname}

		cmd := exec.CommandContext(ctx, path, args...)

		stdout, err := cmd.StdoutPipe()
		if err != nil {
			errc <- err
			return
		}

		if err := cmd.Start(); err != nil {
			errc <- err
			return
		}

		r.state = CONNECTION_LIVE

		scanner := bufio.NewScanner(stdout)
		for scanner.Scan() {
			// scan stdout, send signal on motd
			if strings.HasSuffix(scanner.Text(), `MOTD command.`) {
				break
			}
		}
		if err := scanner.Err(); err != nil {
			errc <- err
		} else {
			errc <- nil
		}

		if err := cmd.Wait(); err != nil {
			errc <- err
		}

		// unreachable, ii will not end
	}()
	return errc
}

func (r *basicService) State() string {
	return r.state
}

// Shutdown IRC connection
func (r *basicService) Close() error {
	r.state = "ConnectionClosed"
	return r.writeToServer("/q bye\n")
}

func (r *basicService) writeToServer(req string) error {
	s, err := os.OpenFile(r.fifo, os.O_WRONLY, 0777)
	if err != nil {
		return err
	}
	s.WriteString(req)
	s.Close()
	return nil
}

func NewIRCDriver(conf *toml.Tree) Service {
	home := conf.Get("development.homedir").(string)
	nick := conf.Get("irc.freenode.nickname").(string)
	srv := conf.Get("irc.freenode.server").(string)
	port := conf.Get("irc.freenode.port").(int64)

	npipe := path.Join(home, "irc", srv, "in")

	return &basicService{
		state:    CONNECTION_INIT,
		nickname: nick,
		server:   srv,
		port:     int(port),
		home:     home,
		fifo:     npipe,
		channels: map[string]int{},
	}
}

type basicService struct {
	state     string
	nickname  string
	server    string
	port      int
	home      string
	fifo      string
	onMessage func([]byte) error
	channels  map[string]int
}
