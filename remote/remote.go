package remote

import (
	"context"
	"regexp"

	"github.com/pelletier/go-toml"
)

type Remote struct {
	Driver Service
	Regex  *regexp.Regexp
}

const IRC_CMD_PATTERN = `^!irc\s+(freenode)\s+(join|quit)\s+(#?[a-z0-9]\S*)`
const TWITCH_CMD_PATTERN = `^!irc\s+(twitch)\s+(join|quit)\s+(#?[a-z0-9]\S*)`
const GITTER_CMD_PATTERN = `^!irc\s+(gitter)\s+(join|quit)\s+([a-z0-9]\S*)`

var remotes = map[string]Remote{} //todo thread-safe

// Carry out job request
func (r *Remote) StartWorker(ctx context.Context, request string, pipe func([]byte) error) {
	// (network specific logic is inside the Driver)

	args := r.Regex.FindStringSubmatch(request)
	if args[2] == "join" {
		if r.Driver.State() != CONNECTION_LIVE {
			errc := r.Driver.Connect(ctx)

			// Pipe to outbound WebSocket for WASM client
			r.Driver.AttachHandler(pipe)

			if esig := <-errc; esig != nil {
				panic(esig)
			}
		}
		err := r.Driver.Join(ctx, args[3])
		if err != nil {
			panic(err) // todo fatal while debugging (malformed channel names fail)
		}
	}
}

func Jobs(conf *toml.Tree) map[string]Remote {
	if len(remotes) < 1 {
		initJobs(conf)
	}
	return remotes
}

// Add the remote for every known job type
// todo this is the only place we write to the map, so concurrent ok for now
func initJobs(conf *toml.Tree) {
	remotes = make(map[string]Remote)

	// Find config of supported jobs
	a := conf.Get("development.accept-jobs").([]interface{})

	for _, v := range a {
		switch v.(string) {
		case "IRC":
			// Initialize the IRC remote control
			remotes[IRC_CMD_PATTERN] = Remote{
				Regex:  regexp.MustCompile(IRC_CMD_PATTERN),
				Driver: NewIRCDriver(conf),
			}
		case "TWITCH":
			// Initialize the Twitch remote control
			remotes[TWITCH_CMD_PATTERN] = Remote{
				Regex:  regexp.MustCompile(TWITCH_CMD_PATTERN),
				Driver: NewTwitchDriver(conf),
			}
		case "GITTER":
			// Initialize the Gitter remote control
			remotes[GITTER_CMD_PATTERN] = Remote{
				Regex:  regexp.MustCompile(GITTER_CMD_PATTERN),
				Driver: NewGitterDriver(conf),
			}
		}
	}
}
