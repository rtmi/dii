# build stage
FROM golang:1.14-alpine3.11 AS build-env
RUN apk update && apk --no-cache add sudo openssl-dev autoconf automake libtool git \
        build-base gcc abuild binutils cmake linux-headers; 

RUN git clone https://git.suckless.org/ii /ii; cd /ii; make;

COPY . /usr/opt/dii
WORKDIR /usr/opt/dii

RUN mkdir dist; \
    mv www dist/public; \
    GOOS=js GOARCH=wasm go build -o dist/public/test.wasm dist/public/main.go; \
    go build -o dist/wssrv *.go; \
    mv dist/public/wasm_exec.html dist/public/index.html; \
    cp config.toml dist/; 



# final stage 
FROM alpine:3.11
COPY --from=build-env /usr/opt/dii/dist/ /var/www/dii/
WORKDIR /var/www/dii
ENTRYPOINT ["/var/www/dii/wssrv"]

ENV SUBGID=1000 \
    SUBUID=1000 \
    NAME=dii \
    USERPASS=dii
COPY --from=build-env /ii/ii /bin/ii
RUN addgroup $NAME; \
    adduser -G $NAME  -s /bin/ash -D $NAME; \
    echo "$NAME:$USERPASS" | /usr/sbin/chpasswd; \
    echo "$NAME ALL=(ALL) ALL" >> /etc/sudoers; \
    chown -R $NAME:$NAME /var/www/dii
USER $NAME
##RUN mkdir /home/$NAME/irc
EXPOSE 8888
