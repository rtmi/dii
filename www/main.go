// WebAssembly-ify a IRC client
//

package main

import (
	"fmt"
	"syscall/js"

	"github.com/patterns/dii/term"
)

const (
	PREFONT_JS = `precacheWebfont`
	CANVAS_ID  = `drawing`
	FONT_NAME  = `RobotoMono`
	FONT_URL   = `url(./RobotoMono-Regular.ttf)`
)

var (
	beforeUnloadCh = make(chan struct{})
	trm            *term.Scripter
)

func main() {
	var (
		keyupCb        = attachCanvasKeypress()
		beforeUnloadCb = js.FuncOf(beforeUnload)
		precacheCb     = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			trm.Send(`Font precaching`)
			return nil
		})
	)

	// js function defined on index.html
	pcfjs := js.Global().Get(PREFONT_JS)
	pcfjs.Invoke(FONT_NAME, FONT_URL, precacheCb)

	js.Global().Call(`addEventListener`, `beforeunload`, beforeUnloadCb)

	// wait for page close
	<-beforeUnloadCh
	keyupCb.Release()
	precacheCb.Release()
	beforeUnloadCb.Release()

	fmt.Println("Done")
}

// attachCanvasKeypress Attaches the key event handler
// (we also perform canvas initialization tasks which may need to be refactor'd)
func attachCanvasKeypress() js.Func {
	if trm == nil {
		trm = term.NewScripter(CANVAS_ID)
	}
	trm.Connect()
	trm.ShowTerminal()

	cb := js.FuncOf(keypressed)
	trm.KeyEventHandler(cb)
	return cb
}

// keypressed is the event handler
// (todo this can quickly become too specialized, can we plug-ify this)
func keypressed(this js.Value, args []js.Value) interface{} {
	var (
		event = args[0]
		k     = event.Get(`key`).String()
	)

	switch k {
	case "ArrowLeft":
		trm.MoveCursorLt()
	case "ArrowRight":
		trm.MoveCursorRt()
	case "q":
		if event.Get(`ctrlKey`).Bool() {
			// Ctl-Q
			beforeUnload(this, args)
		} else {
			trm.BufferKey(k)
		}
	case "Enter":
		trm.Enter()
	case "Shift":
		//noop
	case "Control":
		//noop
	case "Backspace":
		trm.Backspace()
	case "Delete":
		trm.Backspace()
	case "Alt":
		//noop
	default:
		// todo deconstruct into more cases for above switch stmt

		if !event.Get(`ctrlKey`).Bool() {
			trm.BufferKey(k)
		}
	}
	return nil
}

func beforeUnload(this js.Value, args []js.Value) interface{} {
	beforeUnloadCh <- struct{}{}
	return nil
}
