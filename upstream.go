package main

import (
	"context"
	"net/http"
	"strings"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"github.com/patterns/dii/remote"
)

func upstream(ctx context.Context) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			conn, _, _, err := ws.UpgradeHTTP(r, w)
			if err != nil {
				panic(err)
			}

			go func() {
				defer conn.Close()

				for {
					msg, op, err := wsutil.ReadClientData(conn)
					if err != nil {
						panic(err)
					}
					err = wsutil.WriteServerMessage(conn, op, msg)
					if err != nil {
						panic(err)
					}
					fireTriggers(ctx, msg, op, func(m []byte) error {
						return wsutil.WriteServerMessage(conn, ws.OpText, m)
					})
				}
			}()
		})
}

// Run action(s) that are hooked to the data event
func fireTriggers(ctx context.Context, data []byte, op ws.OpCode, pipe func([]byte) error) {

	if op != ws.OpText {
		// Don't care about non-textual data
		return
	}

	// Loop through because it can be possible for
	// multiple actions to be chained to one job msg
	low := strings.ToLower(string(data))
	for _, v := range remote.Jobs(config) {
		if v.Regex.MatchString(low) {
			v.StartWorker(ctx, low, pipe)
		}
	}
}
