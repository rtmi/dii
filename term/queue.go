package term

type queue struct {
	data []string
}

func newQueue() *queue {
	return &queue{data: []string{}}
}

func (s *queue) Enqueue(item string) {
	s.data = append(s.data, item)
}

func (s *queue) Dequeue() (datum string) {
	n := s.Size()
	datum, s.data = s.data[0], s.data[1:n-1]
	return
}

func (s *queue) Size() int {
	return len(s.data)
}

func (s *queue) Clear() {
	s.data = []string{}
}
