package term

import (
	"strings"
	"syscall/js"
)

func (u *Scripter) KeyEventHandler(keydown js.Func) {
	u.canvas.Call(`addEventListener`, `keyup`, keydown)
}

func (u *Scripter) BufferKey(k string) {
	u.inbuf.Push(k)
	u.Write(k)
	if u.xpos < u.width-u.wpx {
		u.MoveCursorRt()
	}
	//else scroll rt
}

func (u *Scripter) Backspace() {
	u.inbuf.Pop()
	if u.xpos > u.wpx {
		u.MoveCursorLt()
		u.blankCursor()
	}

	u.ctx.Set("fillStyle", u.bkgndColor)
	u.ctx.Call("fillRect", u.xpos, u.ypos, u.wpx, u.fontsize)
}

func (u *Scripter) Enter() {
	m := strings.Join(u.inbuf.data, "")
	u.inbuf.Clear()
	if strings.TrimSpace(m) != "" {
		u.Send(m)
	}

	u.xpos = 2
	u.MoveCursor(u.xpos, u.ypos)
	u.blankCommandRow()
}

func (u *Scripter) MoveCursorUp() {
	y := u.ypos - CURSOR_HT
	u.MoveCursor(u.xpos, y)
}
func (u *Scripter) MoveCursorDn() {
	y := u.ypos + CURSOR_HT
	u.MoveCursor(u.xpos, y)
}
func (u *Scripter) MoveCursorLt() {
	x := u.xpos - u.wpx
	u.MoveCursor(x, u.ypos)
}
func (u *Scripter) MoveCursorRt() {
	x := u.xpos + u.wpx
	u.MoveCursor(x, u.ypos)
}
func (u *Scripter) MoveCursor(x int, y int) {
	//u.ctx.Call("clearRect", u.xpos, u.ypos, u.wpx, CURSOR_HT)
	u.xpos = x
	u.ypos = y
	u.ctx.Set("fillStyle", "green")
	u.ctx.Call("fillRect", u.xpos, u.ypos, u.wpx, CURSOR_HT)
}
