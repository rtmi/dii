package term

import (
	"fmt"
)

// Write line of text to room panel
func (u *Scripter) writeToViewp(line string) {
	if !u.viewpBottom() {
		u.viewpln(line)
		u.yrm = u.yrm + u.lineHt
		return
	}

	// Trace the clip area to be the room pane
	u.ctx.Call("beginPath")
	u.ctx.Call("rect", 0, 0, u.width, u.ypos-u.lineHt-2)
	u.ctx.Call("clip")
	u.ctx.Call("save")

	// Translate origin upward to paint the current elements repositioned one row above
	u.ctx.Call("translate", 0, -1*u.lineHt)
	u.ctx.Call("drawImage", u.ctx.Get("canvas"), 0, 0)
	////	u.yrm = u.yrm - u.lineHt

	// Blank the bottom row
	u.ctx.Call("clearRect", 0, u.yrm, u.width, u.lineHt)
	u.ctx.Set("fillStyle", u.bkgndColor)
	u.ctx.Call("fillRect", 0, u.yrm, u.width, u.lineHt)

	u.viewpln(line)
	u.ctx.Call("restore") // apply the clip mask
}

// Check whether the lines of text filled to the bottom of the room panel
func (u *Scripter) viewpBottom() bool {
	if u.yrm < (u.ypos - 2*u.lineHt) {
		return false
	}
	return true
}

// Roomln prints text on the next line of the room panel
func (u *Scripter) viewpln(txt string) {
	f := fmt.Sprintf("%dpx %s", u.fontsize, u.fontname)
	u.ctx.Set("font", f)
	u.ctx.Set("fillStyle", u.fontColor)
	u.ctx.Call("fillText", txt, u.xrm, u.yrm)
}
