package term

import (
	"fmt"
	"strings"
	"syscall/js"

	"github.com/pelletier/go-toml"
)

type Scripter struct {
	width        int
	height       int
	dom          js.Value
	canvas       js.Value
	xpos         int
	ypos         int
	ctx          js.Value
	fontface     js.Value
	wsock        js.Value
	xrm          int
	yrm          int
	wpx          int
	inbuf        *stack
	lineHt       int
	fontsize     int
	fontname     string
	fonturl      string
	fontColor    string
	bkgndColor   string
	commandColor string
	config       *toml.Tree
	url          string
}

// New starts a new UI scripter
func NewScripter(id string) *Scripter {
	d := js.Global().Get(`document`)
	c := d.Call(`getElementById`, id)
	x := c.Call(`getContext`, `2d`)
	s := newStack()
	t := defaults()

	return &Scripter{
		dom:    d,
		canvas: c,
		ctx:    x,
		xpos:   0,
		ypos:   0,
		xrm:    0,
		yrm:    0,
		inbuf:  s,
		config: t,
	}
}

const (
	GRADPRPL  = `rgb(51,0,255)`
	GRADBLUE  = `rgb(0,102,255)`
	CURSOR_HT = 1
)

func (u *Scripter) ShowTerminal() {
	u.sizeTerminal()
	u.calcFontWidth()
	u.wallpaperTerminal()
	u.overlayCommandRow()
}

func (u *Scripter) clearCanvas() {
	u.canvas.Set("width", u.width)
}

func (u *Scripter) LoadFont() {
	f := js.Global().Get("FontFace")
	u.fontface = f.New(u.fontname, u.fonturl)
	r := u.fontface.Call("load")
	cb := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		u.Send("Font ready.")
		return nil
	})
	r.Call("then", cb)
}

func (u *Scripter) calcFontWidth() {
	// Assume font has been loaded.
	f := fmt.Sprintf("%dpx %s", u.fontsize, u.fontname)
	u.ctx.Set("font", f)

	m := u.ctx.Call("measureText", "X")
	u.wpx = m.Get("width").Int()
}

func (u *Scripter) wallpaperTerminal() {

	// lime 218,247,166

	u.ctx.Set("fillStyle", u.bkgndColor)
	u.ctx.Call("fillRect", 0, 0, u.width, u.height)
}

func (u *Scripter) sizeTerminal() {
	window := js.Global()
	h := window.Get("innerHeight").Int() - u.fontsize
	w := window.Get("innerWidth").Int() - u.fontsize
	u.height = h
	u.width = w

	u.canvas.Set("height", h)
	u.canvas.Set("width", w)
	u.ypos = h - u.fontsize/2
	u.xpos = 2
	u.yrm = u.fontsize + 2
	u.xrm = 2
}

func (u *Scripter) overlayCommandRow() {
	topx := 0
	topy := u.height - u.lineHt - 2
	wd := u.width

	gr := u.ctx.Call("createLinearGradient", topx, topy, wd, u.lineHt)

	// Add the color stops.
	gr.Call("addColorStop", 0, GRADPRPL)
	gr.Call("addColorStop", 1, GRADBLUE)

	// Use the gradient for the fillStyle.
	u.ctx.Set("strokeStyle", gr)
	u.ctx.Call("strokeRect", topx, topy, wd, u.lineHt)
}
func (u *Scripter) blankCommandRow() {
	topx := 0
	topy := u.height - u.lineHt - 2
	wd := u.width

	u.ctx.Set("fillStyle", u.bkgndColor)
	u.ctx.Call("fillRect", topx, topy, wd, u.lineHt)
}
func (u *Scripter) blankCursor() {
	topx := u.xpos
	topy := u.ypos - u.fontsize
	wd := u.wpx

	u.ctx.Set("fillStyle", u.bkgndColor)
	u.ctx.Call("fillRect", topx, topy, wd, u.lineHt)
}

// Send enqueues message on WebSocket
func (u *Scripter) Send(msg string) {
	u.wsock.Call(`send`, msg)
}

// Write text in the command row
func (u *Scripter) Write(txt string) {
	f := fmt.Sprintf("%dpx %s", u.fontsize, u.fontname)
	u.ctx.Set("font", f)
	u.ctx.Set("fillStyle", u.commandColor)
	u.ctx.Call("fillText", txt, u.xpos, u.ypos)
}

// Connect opens WebSocket and attaches event handlers
func (u *Scripter) Connect() {
	u.configure()

	w := js.Global().Get(`WebSocket`)
	u.wsock = w.New(u.url)

	addcb := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		ev := args[0]
		m := ev.Get(`data`).String()
		a := strings.Split(m, "\n")

		for _, v := range a {
			u.writeToViewp(v)
		}

		return nil
	})
	u.wsock.Set(`onmessage`, addcb)
}

// Default values until httpd connection can be established
func defaults() *toml.Tree {
	conf, err := toml.Load(DEFAULT_CONFIG)
	if err != nil {
		panic(err)
	}
	return conf
}

// To update conf fields
func (u *Scripter) configure() {
	var (
		fsize int64
		fcol  string
		bcol  string
		ccol  string
		url   string
		fam   []interface{}
	)
	// Keep a flag in the config tree to indicate whether it is safe to read from online config
	confready := u.config.Get("defaults.config-online").(bool)
	if !confready {
		fsize = u.config.Get("defaults.font-size").(int64)
		fcol = u.config.Get("defaults.font-color").(string)
		bcol = u.config.Get("defaults.bkgnd-color").(string)
		ccol = u.config.Get("defaults.command-color").(string)
		url = u.config.Get("defaults.websocket-url").(string)
		fam = u.config.Get("defaults.font-family").([]interface{})
	} else {
		fsize = u.config.Get("development.font-size").(int64)
		fcol = u.config.Get("development.font-color").(string)
		bcol = u.config.Get("development.bkgnd-color").(string)
		ccol = u.config.Get("development.command-color").(string)
		url = u.config.Get("development.websocket-url").(string)
		fam = u.config.Get("development.font-family").([]interface{})
	}
	// Make line height half more than font size to accomodate descenders
	u.lineHt = int(fsize + fsize/2)
	u.fontsize = int(fsize)
	u.fontColor = fcol
	u.bkgndColor = bcol
	u.commandColor = ccol
	u.url = url

	// Maps are not ordered and we only want one, for now
	font := fam[0].([]interface{})
	u.fontname = font[0].(string)
	u.fonturl = font[1].(string)

}

const DEFAULT_CONFIG = `
[defaults]
config-online = false			# Is online conf ready?
websocket-server = ":8888"
websocket-url = "ws://localhost:8888/ws"
canvas-id = "drawing"
font-family = [ ["SpaceMono", "url(./SpaceMono-Regular.ttf)"] ]
font-size = 16
font-color = "rgba(255,87,51,1)"        # Orange text
bkgnd-color = "rgba(0, 0, 0, 1)"        # Black background
command-color = "rgba(255,255,255,1)"   # White command text

`
