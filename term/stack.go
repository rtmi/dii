package term

type stack struct {
	data []string
}

func newStack() *stack {
	return &stack{data: []string{}}
}

func (s *stack) Push(item string) {
	s.data = append(s.data, item)
}

func (s *stack) Pop() (datum string) {
	n := s.Size()
	datum, s.data = s.data[n-1], s.data[:n-1]
	return
}

func (s *stack) Size() int {
	return len(s.data)
}

func (s *stack) Clear() {
	s.data = []string{}
}
