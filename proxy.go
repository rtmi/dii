package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/user"

	"github.com/pelletier/go-toml"
)

var (
	addr    string
	config  *toml.Tree
	ircsrv  string
	ircport int
	ircnick string
	ircchan []string
)

func main() {
	configure()
	wd, err := os.Getwd()
	if err != nil {
		log.Fatalf("can not get os working directory: %v", err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	web := http.FileServer(http.Dir(wd + "/public"))

	http.Handle("/", web)
	http.Handle("/public/", http.StripPrefix("/public/", web))
	http.Handle("/ws", upstream(ctx))

	log.Printf("proxy is listening on %q", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}

func configure() {
	var err error
	config, err = toml.LoadFile("config.toml")
	if err != nil {
		log.Fatal(err)
	}

	cu, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	config.Set("development.homedir", cu.HomeDir)
	addr = config.Get("development.websocket-server").(string)
}
