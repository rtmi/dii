module github.com/patterns/dii

go 1.14

require (
	github.com/gobwas/httphead v0.0.0-20180130184737-2c6c146eadee // indirect
	github.com/gobwas/pool v0.2.0 // indirect
	github.com/gobwas/ws v1.0.3
	github.com/pelletier/go-toml v1.7.0
	golang.org/x/sys v0.0.0-20200430082407-1f5687305801 // indirect
)
