# dii

WebAssembly ii.  A minimalist FIFO named pipes based IRC client by
 [Suckless](https://suckless.org)



## Quickstart

1. git clone https://github.com/patterns/dii
2. Optionally, edit the configuration:

```
cp dii/config-example.toml dii/config.toml
vi dii/config.toml

```

3. docker build -t dii dii
4. docker run -ti --rm -e TWITCHTOKEN=$TOKEN_VAL dii
5. Visit localhost:8888
6. Connect to Freenode:

```
!irc freenode join #bot-test
!irc twitch join #cosmos

```



## Credits

WebSocket example by
 [Sergey Kamardin](https://github.com/gobwas/ws)
 ([LICENSE](https://github.com/gobwas/ws/blob/master/LICENSE))

Go's WASM
 [Start Files](https://github.com/golang/go/tree/master/misc/wasm)

[Suckless Tools](https://suckless.org)

Space Mono and Noto from
 [Google Web Fonts](https://fonts.google.com)

